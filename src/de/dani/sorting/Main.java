package de.dani.sorting;

import static de.dani.sorting.Util.*;

public class Main {
    int list[];

    public Main(){
        int[] list = randomize(populate(new int[100]));
        System.out.println("Initial List");
        barChartHorizontal(list);
        System.out.println(Sorting.bubbleSort(copyList(list)));
    }

    public static void main(String[] args){
        new Main();
    }


}
