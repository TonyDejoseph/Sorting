package de.dani.sorting;

import java.util.*;

import static de.dani.sorting.Util.*;

public class Sorting {
    public static List<Long> statistics = new ArrayList<Long>();

    public static Map<String, String> bogoSort(int[] list){
        Map<String, String> map = new HashMap<String, String>();

        long start = System.nanoTime();
        int shuffle=1;
        for (; !orderd(list);shuffle++) {
            randomize(list);
        }
        statistics.add((long)shuffle);

            long end = System.nanoTime();
        System.out.println("Sorted List");
        barChartHorizontal(list);

        long ns = (end-start);
        long µs = (end-start)/1000;
        long ms = (end-start)/1000000;
        long s  = (end-start)/1000000000;

        map.put("Time", "BogoSort took " +ns +" ns, " +µs +"µs, "+ms +"ms, "+s +"s");
        map.put("Shuffles", "BogoSort took " +shuffle +" shuffles.");
        String stat = "";
        for(long l: statistics){
            stat += l;
        }
        map.put("Statistics", stat);
        return map;
    }


    public static Map<String, String> bubbleSort(int[] list){
        Map<String, String> map = new HashMap<String, String>();

            long start = System.nanoTime();
        for (int i = 0; i < list.length - 1; i++) {
            for (int j = 0; j < list.length - i - 1; j++) {
                if(list[j] < list[j+1]){
                    swap(list, j, j+1);
                }
            }
        }
            long end = System.nanoTime();
        System.out.println("Sorted List");
        barChartHorizontal(list);
        long ns = (end-start);
        long µs = (end-start)/1000;
        long ms = (end-start)/1000000;
        long s  = (end-start)/1000000000;

        map.put("Time", "BubbleSort took " +ns +" ns, " +µs +"µs, "+ms +"ms, "+s +"s");
        return map;
    }
}
