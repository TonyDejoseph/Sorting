package de.dani.sorting;

import java.util.Random;

public class Util {
    public static boolean orderd(int[] list){
        for(int i=1;i<list.length;i++)
            if(list[i]<list[i-1])
                return false;
        return true;
    }

    public static int[] populate(int[] list){
        for(int i = 0; i < list.length;i++){
            list[i] = new Random().nextInt(30);
        }
        return list;
    }

    public static int[] randomize(int[] list){
        int i=list.length-1;
        while(i>0)
            list = swap(list,i--,(int)(Math.random()*i));
        return list;
    }

    public static void printList(int[] list){
        for(int i: list){
            System.out.print(i +" | ");
        }
        System.out.print("\n");
    }

    public static void barChart(int... arr){
        for(int i: arr){
            for(int j = 0; j < i; j++){
                  System.out.print("-");
            }
            System.out.print("\n");
        }
    }

    public static void barChartHorizontal(int... arr){
        int j = 0;
        while(j < getMax(arr)){
            for(int i: arr){
                if(i > j){
                    System.out.print("| ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.print("\n");

           j++;
        }
    }

    public static int[] swap(int[] list,int i,int j){
        int temp=list[i];
        list[i]=list[j];
        list[j]=temp;
        return list;
    }

    public static int[] copyList(int[] list){
        int[] newList = new int[list.length];
        for(int i = 0; i < list.length;i++){
            newList[i] = list[i];
        }
        return newList;
    }

    public static int getMax(int... arr){
        int max = 0;
        for(int i: arr){
            if(i > max){
                max = i;
            }
        }
        return max;
    }

}
